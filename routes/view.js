var express = require('express');
var router = express.Router();

/* GET submission page. */
router.get('/', function(req, res) {
   
  // Bring in var db
  var db = req.db;
  
  // Get the results from the database.
  db.collection('drinks').find().toArray((err, result) => {
    // Render our page
    res.render('view', { title: 'DrinkApp: Viewing Database', page_id: 'view', drinks : result, environment: req.environment, host: req.node_hostname });
  });
});

router.get('/drinks/all', function(req, res) {
    var db = req.db;
    
    // Get the results from the database.
    db.collection('drinks').find().toArray((err, result) => {
      // Render our page
      //res.render('view', { title: 'DrinkApp: Viewing Database', page_id: 'view', drinks : result });
      res.json(result);
  });
});


module.exports = router;
