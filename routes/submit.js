var express = require('express');
var router = express.Router();

/* GET submission page. */
router.get('/', function(req, res) {
  res.render('submit', { title: 'DrinkApp: Submit', page_id: 'submit', environment: req.environment, host: req.node_hostname });
});

router.post('/', function(req, res) {
    
    // Bring in var db
    var db = req.db;

    // Save the form data to our database.
    db.collection('drinks').save(req.body, (err, results) => {
        if (err) return console.log(err);
        res.render('submit_ty', { name : req.body.name, drink : req.body.drink, environment: req.environment, host: req.node_hostname });
    })
    
});

module.exports = router;
