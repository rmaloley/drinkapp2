var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'DrinkApp: Index', page_id: 'index', environment: req.environment, host: req.node_hostname });
});

module.exports = router;
