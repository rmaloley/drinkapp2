var express = require('express');
var router = express.Router();

/* GET admin page. */
router.get('/', function(req, res) {
  var db = req.db;
  db.collection('drinks').count(function (err, count){
    res.render('admin', { title: 'DrinkApp: Admin Console', page_id: 'admin', numDocs : count, environment: req.environment, host: req.node_hostname });
  });
});

// We are gonna delete our database! 
router.post('/reset', function(req, res) {
    
    // Bring in var db
    var db = req.db;
    //res.send('reset');
    // DB command to drop everything from the database.
    db.collection('drinks').deleteMany({}, (err) => {
        if (err) return console.log(err);
         res.redirect('/');
    })
});

module.exports = router;
