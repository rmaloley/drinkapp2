var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('dotenv').config(); // Use .env to designate specific environment variables.

// Define and get our server hostname.
 var os = require("os");
 var node_hostname = os.hostname();

// Require MongoClient
const MongoClient = require('mongodb').MongoClient;

// Define variable to hold our db access.
var db;

// Define and require our route files.
var index = require('./routes/index');
var submit = require('./routes/submit');
var view = require('./routes/view');
var admin = require('./routes/admin');

// We will use ExpressJS
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.locals.pretty = true; // Make pretty HTML

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Define our MongoDB URI
var mongo_uri = "mongodb://" + process.env.DB_HOST + "/test";

// Connect to MongoDB
MongoClient.connect(mongo_uri, (err, database) => {
  if (err) return console.log(err);
  db = database;
});

// Make our db accessible to our router
app.use(function(req,res,next){
    
    // Pass our database into all routes.
    req.db = db;

    // Pass our environment into all routes.
    req.environment = process.env.ENVIRONMENT;

    // Pass our node_hostname into all routes.
    req.node_hostname = node_hostname;

    next();
});
app.use('/', index);
app.use('/submit', submit);
app.use('/view', view);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
